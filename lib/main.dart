import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:uzneftegaz_inspection/utils/shared_pref_helper.dart';

import 'constants/color_constants.dart';
import 'constants/route_constants.dart';
import 'utils/localization_util.dart';
import 'utils/logger/logger.dart';
import 'utils/navigator/locator.dart';
import 'utils/navigator/main_route_generator.dart';
import 'utils/navigator/navigation_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPrefHelper.init();
  setupLocator();
  setUpLogging();
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ),
  );

  runApp(EasyLocalization(
      supportedLocales: LocalizationUtil.supportedLocales(),
      path: 'assets/locales',
      fallbackLocale: Locale('ru', 'RU'),
      child: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final NavigationService navigationService = locator<NavigationService>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    SharedPrefHelper.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(1526, 2048),
      allowFontScaling: false,
      child: MaterialApp(
        builder: (context, child) {
          return ScrollConfiguration(
            behavior: MyScrollBehavior(),
            child: child,
          );
        },
        title: 'Узнефтегаинспекция',
        debugShowCheckedModeBanner: false,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        theme: ThemeData(
          appBarTheme: AppBarTheme(
              color: AppColors.primaryColor,
              iconTheme: IconThemeData(color: Colors.white)),
          accentColor: AppColors.primaryColor,
          scaffoldBackgroundColor: AppColors.backgroundColor,
          brightness: Brightness.dark,
          canvasColor: Colors.white,
          iconTheme: IconThemeData(color: AppColors.iconColor),
        ),
        initialRoute: AppRoutes.launchPage,
        onGenerateRoute: MainRouteGenerator.generateRoute,
        navigatorKey: locator<NavigationService>().navigatorKey,
      ),
    );
  }
}

class MyScrollBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}
