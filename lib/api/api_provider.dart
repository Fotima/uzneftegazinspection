import 'dart:io';
import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart' as http;
import 'package:uzneftegaz_inspection/services/form_service.dart';
import 'custom_converter.dart';

class ApiProvider {
  static ChopperClient _client;
  static FormService formApi;

  ///Services
  static create({String token}) {
    _client = ChopperClient(
      client: http.IOClient(
        HttpClient()..connectionTimeout = Duration(seconds: 40),
      ),
      services: [
        FormService.create(),
      ],
      interceptors: getInterceptors(token),
      converter: CustomDataConverter(),
    );

    formApi = _client.getService<FormService>();
  }

  static List getInterceptors(token) {
    List interceptors = List();
    String language = 'ru';
    // String language = AppTranslations.of(context).currentLanguage ?? 'ru';

    interceptors.add(HttpLoggingInterceptor());
    if (token != null) {
      debugPrint('Current token is $token');
      interceptors.add(HeadersInterceptor({
        HttpHeaders.authorizationHeader: 'Bearer $token',
        HttpHeaders.acceptHeader: 'application/json',
        HttpHeaders.contentTypeHeader: 'application/json',
        'lang': language,
      }));
    }
    return interceptors;
  }

  static dispose() {
    _client.dispose();
  }
}
