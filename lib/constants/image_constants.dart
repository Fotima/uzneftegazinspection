abstract class AppImages {
  static const images = 'assets/images';
  static const icons = 'assets/icons';
  static const flare = 'assets/flare';

  static const emblem = '$images/emblem.png';

  static const russian_flag = '$icons/russian_flag.svg';
  static const uzbek_flag = '$icons/uzbek_flag.svg';
  static const error_icon = '$icons/error_icon.svg';

  static const check = '$flare/check.flr';
}
