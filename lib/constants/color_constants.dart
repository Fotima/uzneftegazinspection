import 'package:flutter/material.dart';

abstract class AppColors {
  static const primaryColor = Color(0xFF2958a1);
  static const primaryAlt = Color(0xFF176aba);
  static const backgroundColor = Color(0xFFFCFCFC);
  static const focusedBorder = Color(0xFF140F1E);
  static const accentColor = Color(0xFFff7e2f);
  static const iconColor = Color(0xFFBABABA);
  static const lightGrey = Color(0xFF9C99A0);
  static const darkGrey = Color(0xFF3C3C43);
  static const buttonGrayText = Color(0xFFBABABA);
  static const border = Color(0xFFD0D0D0);
  static const red = Color(0xFFd32f2f);
}
