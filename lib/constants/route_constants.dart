abstract class AppRoutes {
  static const home = '/homePage';
  static const launchPage = '/';
  static const reviewForm = '/reviewFormPage';
}
