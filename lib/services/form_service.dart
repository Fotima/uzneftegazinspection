import 'package:chopper/chopper.dart';
import 'package:uzneftegaz_inspection/models/api/review_post.dart';

part 'form_service.chopper.dart';

@ChopperApi(baseUrl: 'server_name')
abstract class FormService extends ChopperService {
  static FormService create([ChopperClient client]) => _$FormService(client);

  @Post(path: 'http://bot.slash.uz/uzneft/api/form')
  Future<Response> sendReview(
    @Body() ReviewPost body,
  );
}
