// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$FormService extends FormService {
  _$FormService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = FormService;

  @override
  Future<Response<dynamic>> sendReview(ReviewPost body) {
    final $url = 'http://bot.slash.uz/uzneft/api/form';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }
}
