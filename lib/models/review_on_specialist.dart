class ReviewOnSpecialist {
  int id;
  String fullName;
  String comment;
  double rate;
  ReviewOnSpecialist({
    this.id,
    this.comment,
    this.fullName,
    this.rate = 0,
  });
}
