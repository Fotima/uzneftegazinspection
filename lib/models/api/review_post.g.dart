// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review_post.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReviewPost _$ReviewPostFromJson(Map<String, dynamic> json) {
  return ReviewPost(
    city: json['city'] as String,
    employee: (json['employee'] as List)
        ?.map((e) =>
            e == null ? null : Employee.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    feedbackType: json['feedback_type'] as int,
    isAnonymous: json['is_anonymous'] as bool,
    serviceType: json['service_type'] as int,
    companyName: json['company_name'] as String,
    firstName: json['firstname'] as String,
    lastName: json['lastname'] as String,
    middleName: json['middlename'] as String,
    phone: json['phone'] as String,
  );
}

Map<String, dynamic> _$ReviewPostToJson(ReviewPost instance) =>
    <String, dynamic>{
      'city': instance.city,
      'is_anonymous': instance.isAnonymous,
      'service_type': instance.serviceType,
      'feedback_type': instance.feedbackType,
      'employee': instance.employee,
      'firstname': instance.firstName,
      'lastname': instance.lastName,
      'middlename': instance.middleName,
      'phone': instance.phone,
      'company_name': instance.companyName,
    };

Employee _$EmployeeFromJson(Map<String, dynamic> json) {
  return Employee(
    comment: json['comment'] as String,
    firstName: json['firstname'] as String,
    lastName: json['lastname'] as String,
    mark: json['mark'] as int,
    middleName: json['middlename'] as String,
  );
}

Map<String, dynamic> _$EmployeeToJson(Employee instance) => <String, dynamic>{
      'firstname': instance.firstName,
      'lastname': instance.lastName,
      'middlename': instance.middleName,
      'mark': instance.mark,
      'comment': instance.comment,
    };
