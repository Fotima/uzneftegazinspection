import 'package:json_annotation/json_annotation.dart';
part 'review_post.g.dart';

@JsonSerializable()
class ReviewPost {
  String city;

  @JsonKey(name: 'is_anonymous')
  bool isAnonymous;

  @JsonKey(name: 'service_type')
  int serviceType;

  @JsonKey(name: 'feedback_type')
  int feedbackType;

  List<Employee> employee;

  @JsonKey(name: 'firstname')
  String firstName;

  @JsonKey(name: 'lastname')
  String lastName;

  @JsonKey(name: 'middlename')
  String middleName;

  String phone;

  @JsonKey(name: 'company_name')
  String companyName;

  ReviewPost({
    this.city,
    this.employee,
    this.feedbackType,
    this.isAnonymous,
    this.serviceType,
    this.companyName,
    this.firstName,
    this.lastName,
    this.middleName,
    this.phone,
  });

  factory ReviewPost.fromJson(Map<String, dynamic> json) =>
      _$ReviewPostFromJson(json);

  Map<String, dynamic> toJson() => _$ReviewPostToJson(this);
}

@JsonSerializable()
class Employee {
  @JsonKey(name: 'firstname')
  String firstName;

  @JsonKey(name: 'lastname')
  String lastName;

  @JsonKey(name: 'middlename')
  String middleName;

  int mark;

  String comment;

  Employee({
    this.comment,
    this.firstName,
    this.lastName,
    this.mark,
    this.middleName,
  });

  factory Employee.fromJson(Map<String, dynamic> json) =>
      _$EmployeeFromJson(json);

  Map<String, dynamic> toJson() => _$EmployeeToJson(this);
}
