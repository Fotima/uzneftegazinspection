import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:uzneftegaz_inspection/constants/color_constants.dart';
import 'package:uzneftegaz_inspection/constants/constants.dart';
import 'package:uzneftegaz_inspection/constants/image_constants.dart';
import 'package:uzneftegaz_inspection/constants/route_constants.dart';
import 'shared/app_text.dart';
import 'shared/default_button.dart';
import 'shared/language_picker.dart';

class LaunchPage extends StatefulWidget {
  @override
  _LaunchPageState createState() => _LaunchPageState();
}

class _LaunchPageState extends State<LaunchPage> {
  List<String> regions = [];
  int selectedRegion = 0;
  @override
  void initState() {
    setDataForDropDown();
    super.initState();
  }

  setDataForDropDown() {
    regions = [
      tr('tashkent'),
      tr('tashkentoblast'),
      tr('andijan'),
      tr('buhara'),
      tr('djizak'),
      tr('kashkadarya'),
      tr('navoi'),
      tr('namangan'),
      tr('samarkand'),
      tr('surxandarya'),
      tr('sirdarya'),
      tr('fergana'),
      tr('xorezm'),
      tr('karakalpak'),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverToBoxAdapter(
          child: Container(
              padding: EdgeInsets.symmetric(vertical: 82.h, horizontal: 32.w),
              child: Align(
                alignment: Alignment.centerRight,
                child: LanguagePicker(
                  onLanguageUpdate: () {
                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      setDataForDropDown();
                      setState(() {});
                    });
                  },
                ),
              )),
        ),
        SliverFillRemaining(
          hasScrollBody: false,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 82.h, horizontal: 32.w),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        width: MediaQuery.of(context).size.width / 5,
                        alignment: Alignment.centerRight,
                        child: Image.asset(
                          AppImages.emblem,
                        )),
                    SizedBox(width: 16.w),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppText(
                          tr('uznefteinspekciya').toUpperCase(),
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: Constants.textSize48,
                        ),
                        SizedBox(height: 12.h),
                        AppText(
                          tr('logoTitle').toUpperCase(),
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.w600,
                          fontSize: Constants.textSize30,
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(height: 82.h),
                AppText(
                  tr('welcomeText'),
                  color: AppColors.darkGrey,
                  textAlign: TextAlign.center,
                  fontSize: Constants.textSize55,
                ),
                SizedBox(height: 82.h),
                RegionDropDown(
                  regions: regions,
                  onChanged: (String value) {
                    selectedRegion = regions.indexOf(value);
                  },
                ),
                SizedBox(height: 82.h),
                DefaultFullButton(
                  onPressed: () {
                    Navigator.pushNamed(context, AppRoutes.reviewForm,
                        arguments: regions[selectedRegion]);
                  },
                  text: tr('addReview'),
                  fontSize: Constants.textSize50,
                ),
              ],
            ),
          ),
        ),
      ]),
    );

    //   SingleChildScrollView(
    //     padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 82.h),
    //     child: Column(
    //       crossAxisAlignment: CrossAxisAlignment.center,
    //       children: [
    //         Align(alignment: Alignment.centerRight, child: LanguagePicker()),
    //         SizedBox(height: 84.h),
    //         Column(
    //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //           children: [
    //             Row(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: [
    //                 Container(
    //                     width: MediaQuery.of(context).size.width / 5,
    //                     alignment: Alignment.centerRight,
    //                     child: Image.asset(
    //                       AppImages.emblem,
    //                     )),
    //                 SizedBox(width: 16.w),
    //                 Column(
    //                   crossAxisAlignment: CrossAxisAlignment.start,
    //                   children: [
    //                     AppText(
    //                       'Узнефтегазинспекция'.toUpperCase(),
    //                       color: AppColors.primaryColor,
    //                       fontWeight: FontWeight.bold,
    //                       fontSize: Constants.textSize48,
    //                     ),
    //                     SizedBox(height: 12.h),
    //                     AppText(
    //                       tr('logoTitle').toUpperCase(),
    //                       color: AppColors.primaryColor,
    //                       fontWeight: FontWeight.w600,
    //                       fontSize: Constants.textSize30,
    //                     )
    //                   ],
    //                 )
    //               ],
    //             ),
    //             AppText(
    //               tr('welcomeText'),
    //               color: AppColors.darkGrey,
    //               textAlign: TextAlign.center,
    //               fontSize: Constants.textSize55,
    //             ),
    //             RegionDropDown(
    //               regions: regions,
    //             ),
    //             DefaultFullButton(
    //               onPressed: () {
    //                 Navigator.pushNamed(context, AppRoutes.reviewForm);
    //               },
    //               text: tr('addReview'),
    //               fontSize: Constants.textSize50,
    //             ),
    //           ],
    //         )
    //       ],
    //     ),
    //   ),
    // );
  }
}

class RegionDropDown extends StatelessWidget {
  final List<String> regions;
  final Function onChanged;

  RegionDropDown({
    this.regions,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Container(),
        ),
        Expanded(
          flex: 5,
          child: GestureDetector(
            onTap: () {},
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: AppColors.border),
                  borderRadius: BorderRadius.circular(12)),
              child: Row(
                children: [
                  Expanded(
                    child: DropdownButtonFormField(
                        decoration: InputDecoration(
                          suffixIcon: Icon(
                            Icons.keyboard_arrow_down,
                            color: AppColors.focusedBorder.withOpacity(.6),
                            size: 32,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(30.0),
                            ),
                          ),
                        ),
                        onChanged: (value) {
                          onChanged(value);
                        },
                        value: regions.first,
                        items: regions
                            .map((e) => DropdownMenuItem(
                                  child: AppText(
                                    e,
                                    fontSize: Constants.textSize45,
                                  ),
                                  value: e,
                                ))
                            .toList()),
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(),
        ),
      ],
    );
  }
}
