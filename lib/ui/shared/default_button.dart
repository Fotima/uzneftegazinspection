import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:uzneftegaz_inspection/constants/color_constants.dart';
import 'package:uzneftegaz_inspection/constants/constants.dart';
import 'package:uzneftegaz_inspection/ui/shared/app_text.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DefaultFullButton extends StatelessWidget {
  final String text;
  final Function onPressed;
  final bool enabled;
  final double width;
  final FontWeight fontWeight;
  final Gradient gradient;
  final Widget child;
  final double fontSize;

  const DefaultFullButton({
    Key key,
    this.text,
    this.onPressed,
    this.enabled = true,
    this.fontWeight = FontWeight.w800,
    this.width,
    this.gradient,
    this.fontSize,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.all(0),
      onPressed: enabled ? onPressed : null,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        padding: EdgeInsets.only(left: 12, right: 12),
        decoration: BoxDecoration(
          // color: Thm.of(context).buttonGray,
          borderRadius: BorderRadius.circular(12),
          gradient: enabled
              ? gradient ??
                  LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      AppColors.primaryColor,
                      AppColors.primaryAlt,
                    ],
                  )
              : null,
        ),
        height: 150.h,
        width: width ?? MediaQuery.of(context).size.width * 0.9,
        child: Center(
          child: child ??
              AppText(
                text,
                color: enabled ? Colors.white : AppColors.buttonGrayText,
                fontWeight: FontWeight.w600,
                fontSize: fontSize ?? Constants.textSize14,
              ),
        ),
      ),
    );
  }
}
