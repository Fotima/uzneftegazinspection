import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:uzneftegaz_inspection/constants/color_constants.dart';

class DefaultCheckBox extends StatelessWidget {
  final bool checked;
  final double size;

  DefaultCheckBox({@required this.checked, this.size});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size ?? 70.w,
      width: size ?? 70.w,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(
          width: checked ? 3 : 1,
          color: checked ? Colors.white : AppColors.focusedBorder,
        ),
        shape: BoxShape.circle,
      ),
      child: AnimatedSwitcher(
        duration: Duration(milliseconds: 150),
        switchInCurve: Curves.easeIn,
        transitionBuilder: (child, animation) {
          return ScaleTransition(
            scale: animation,
            child: child,
          );
        },
        child: checked
            ? Container(
                height: 35.w,
                width: 35.w,
                decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.white,
                        blurRadius: 1,
                        spreadRadius: 0.5,
                      )
                    ]),
              )
            : SizedBox.shrink(),
      ),
    );
  }
}
