import 'package:easy_localization/easy_localization.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:uzneftegaz_inspection/constants/color_constants.dart';
import 'package:uzneftegaz_inspection/constants/constants.dart';
import 'package:uzneftegaz_inspection/ui/shared/app_text.dart';

void showMessage(String message, bool isError, BuildContext context,
    {String titleText}) async {
  Flushbar(
    icon: isError
        ? Icon(
            Icons.error,
            color: Colors.white,
          )
        : Icon(Icons.check_circle, color: Colors.white),
    margin: EdgeInsets.fromLTRB(15, 50, 15, 0),
    flushbarPosition: FlushbarPosition.TOP,
    forwardAnimationCurve: Curves.easeInOut,
    borderRadius: 8.0,
    messageText: AppText(
      message.isEmpty ? tr('errorMessage') : message,
      fontSize: Constants.textSize40,
      color: Colors.white,
    ),
    // message: message.isEmpty ? tr('errorMessage') : message,
    title: titleText ?? null,
    duration: Duration(seconds: 4),
    isDismissible: true,
    backgroundColor: isError ? Colors.red : AppColors.accentColor,
  )..show(context);
}
