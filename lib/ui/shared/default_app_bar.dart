import 'dart:io';

import 'package:flutter/material.dart';
import 'package:uzneftegaz_inspection/constants/color_constants.dart';
import 'package:uzneftegaz_inspection/ui/shared/language_picker.dart';
import 'app_text.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DefaultAppBar extends StatelessWidget implements PreferredSize {
  final Function onBackPressed;
  final String title;
  final bool showIcon;

  DefaultAppBar({
    @required this.onBackPressed,
    this.title,
    this.showIcon = true,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      height: preferredSize.height + MediaQuery.of(context).padding.top,
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      color: AppColors.primaryColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          showIcon
              ? IconButton(
                  onPressed: () {
                    onBackPressed();
                  },
                  icon: Icon(
                      Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                      color: Colors.white),
                )
              : Container(),
          AppText(
            title,
            color: Colors.white,
            fontSize: 22,
            fontWeight: FontWeight.w600,
          ),
          Container(
              padding: EdgeInsets.only(
                right: 32.w,
              ),
              child: LanguagePicker())
        ],
      ),
    );
  }

  @override
  Widget get child => throw UnimplementedError();

  @override
  Size get preferredSize => Size.fromHeight(AppBar().preferredSize.height + 10);
}
