import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:uzneftegaz_inspection/constants/color_constants.dart';
import 'package:uzneftegaz_inspection/constants/constants.dart';

class DefaultTextField extends StatelessWidget {
  final bool isRequired;
  TextEditingController controller;
  final String labelText;
  final String hintText;
  final String errorText;
  final bool showErrorText;
  final bool enabled;
  final bool showCursor;
  final FocusNode focusNode;
  final TextInputType keyboardType;
  final bool showRequiredSign;
  final bool password;
  final Function onFieldSubmitted;
  final TextInputAction textInputAction;
  final Function onChanged;
  final Widget leadingIcon;
  final Widget trailingWidget;
  final String leadingText;
  final List<TextInputFormatter> inputFormatters;
  final FormFieldValidator<String> customValidator;
  final VoidCallback onTap;
  final Function onSaved;
  final Color cursorColor;
  final int maxLines;
  final String initialValue;
  final String descriptionText;
  final bool isPhoneTextField;
  final int maxLength;
  final TextCapitalization textCapitalization;
  final bool underlineInputBorder;
  final EdgeInsetsGeometry contentPadding;
  final double radius;
  final bool clearIfNulll;
  DefaultTextField({
    Key key,
    this.isRequired = false,
    @required this.controller,
    this.labelText,
    this.leadingIcon,
    this.password = false,
    this.hintText,
    this.errorText,
    this.showErrorText = true,
    this.enabled = true,
    this.showCursor = true,
    this.inputFormatters,
    this.leadingText,
    this.onChanged,
    this.onFieldSubmitted,
    this.onTap,
    this.trailingWidget,
    this.onSaved,
    this.showRequiredSign = false,
    this.textInputAction = TextInputAction.done,
    this.keyboardType = TextInputType.text,
    this.customValidator,
    this.cursorColor = AppColors.primaryColor,
    this.maxLines,
    this.initialValue,
    this.focusNode,
    this.descriptionText = '',
    this.isPhoneTextField = false,
    this.maxLength,
    this.textCapitalization,
    this.underlineInputBorder = false,
    this.contentPadding,
    this.radius = 12,
    this.clearIfNulll = false,
  }) : super(key: key);
  setText() {
    if (controller == null && initialValue != null) {
      controller = TextEditingController();
      controller.text = initialValue;
    } else if (controller == null && initialValue == null && clearIfNulll) {
      controller = TextEditingController();
      controller.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    setText();
    return GestureDetector(
      onTap: onTap,
      child: Container(
        // decoration: BoxDecoration(
        //   border: underlineInputBorder
        //       ? null
        //       : Border.all(
        //           color: controller?.text != '' && errorText == null
        //               ? AppColors.focusedBorder
        //               : Colors.transparent,
        //         ),
        //   borderRadius: BorderRadius.circular(radius),
        // ),
        child: TextFormField(
          focusNode: focusNode,
          validator: (text) {
            if (isRequired && text.isEmpty) {
              return tr('requiredField');
            }
            if (customValidator != null) {
              return customValidator(text);
            }
            return null;
          },
          // onFieldSubmitted: (String value) {
          //   if (onFieldSubmitted != null) {
          //     onFieldSubmitted(value);
          //   }
          // },
          onChanged: (String value) {
            if (onChanged != null) {
              onChanged(value);
            }
          },
          // onSaved: (String value) {
          //   if (onSaved != null) {
          //     onSaved(value);
          //   }
          // },
          maxLength: maxLength,

          textCapitalization: textCapitalization ?? TextCapitalization.none,
          autovalidateMode: AutovalidateMode.disabled,
          textInputAction: textInputAction,
          controller: controller,
          keyboardType: keyboardType,
          inputFormatters: inputFormatters ?? [],
          obscureText: password,
          cursorColor: cursorColor,
          enabled: enabled,

          showCursor: showCursor,
          readOnly: onTap != null,
          onTap: onTap,
          style: TextStyle(
            color: Colors.black,
            fontSize: Constants.textSize36,
          ),
          maxLines: maxLines,
          decoration: InputDecoration(
            prefixIcon: leadingIcon,
            contentPadding: contentPadding ??
                EdgeInsets.symmetric(vertical: 42.h, horizontal: 32.w),
            suffix: trailingWidget,
            prefixText: leadingText,
            hintText: '$hintText${isRequired ? '*' : ''}',
            hintStyle: TextStyle(
              color: AppColors.lightGrey,
              fontSize: Constants.textSize36,
            ),
            focusedBorder: underlineInputBorder
                ? UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.focusedBorder, width: 1.0),
                  )
                : OutlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.focusedBorder, width: 1.0),
                    borderRadius: BorderRadius.circular(radius),
                  ),
            enabledBorder: underlineInputBorder
                ? UnderlineInputBorder(
                    borderSide: BorderSide(color: AppColors.border, width: 1.0),
                  )
                : OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.border, width: 1.0),
                    borderRadius: BorderRadius.circular(radius),
                  ),
            border: underlineInputBorder
                ? UnderlineInputBorder(
                    borderSide: BorderSide(color: AppColors.border, width: 1.0),
                  )
                : OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.border, width: 1.0),
                    borderRadius: BorderRadius.circular(radius),
                  ),
            disabledBorder: underlineInputBorder
                ? UnderlineInputBorder(
                    borderSide: BorderSide(color: AppColors.border, width: 1.0),
                  )
                : OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.border, width: 1.0),
                    borderRadius: BorderRadius.circular(radius),
                  ),
            errorText: showErrorText ? errorText : null,
            focusedErrorBorder: underlineInputBorder
                ? UnderlineInputBorder(
                    borderSide: BorderSide(color: AppColors.border, width: 1.0),
                  )
                : OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.border, width: 1.0),
                    borderRadius: BorderRadius.circular(radius),
                  ),
            errorBorder: underlineInputBorder
                ? UnderlineInputBorder(
                    borderSide: BorderSide(color: AppColors.border, width: 1.0),
                  )
                : OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.border, width: 1.0),
                    borderRadius: BorderRadius.circular(radius),
                  ),
          ),
        ),
      ),
    );
  }
}
