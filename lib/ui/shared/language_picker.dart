import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:uzneftegaz_inspection/constants/color_constants.dart';
import 'package:uzneftegaz_inspection/constants/image_constants.dart';
import 'package:uzneftegaz_inspection/utils/localization_util.dart';

class LanguagePicker extends StatefulWidget {
  final VoidCallback onLanguageUpdate;
  LanguagePicker({this.onLanguageUpdate});

  @override
  _LanguagePickerState createState() => _LanguagePickerState();
}

class _LanguagePickerState extends State<LanguagePicker> {
  @override
  Widget build(BuildContext context) {
    final String currentLag = LocalizationUtil.currentLangCode(context);
    return GestureDetector(
      onTap: () {
        LocalizationUtil.updateLocal(context, currentLag == 'ru' ? 'uz' : 'ru');
        if (widget.onLanguageUpdate != null) {
          widget.onLanguageUpdate();
        }
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 8.h, horizontal: 16.w),
        height: 100.w,
        width: 100.w,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: AppColors.focusedBorder, width: .5),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(50.w),
          child: SvgPicture.asset(
            currentLag == 'ru' ? AppImages.russian_flag : AppImages.uzbek_flag,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
