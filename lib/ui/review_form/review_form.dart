import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uzneftegaz_inspection/bloc/review_bloc.dart';
import 'package:uzneftegaz_inspection/constants/color_constants.dart';
import 'package:uzneftegaz_inspection/constants/constants.dart';
import 'package:uzneftegaz_inspection/models/api/review_post.dart';
import 'package:uzneftegaz_inspection/ui/review_form/corruption_part.dart';
import 'package:uzneftegaz_inspection/ui/review_form/review_form_part2.dart';
import 'package:uzneftegaz_inspection/ui/review_form/specialist_rating_part.dart';
import 'package:uzneftegaz_inspection/ui/shared/app_text.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_app_bar.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_button.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_check_box.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_text_field.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'final_part.dart';

class ReviewForm extends StatefulWidget {
  final String city;

  ReviewForm({@required this.city});
  @override
  _ReviewFormState createState() => _ReviewFormState();
}

class _ReviewFormState extends State<ReviewForm> {
  PageController pageController = PageController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  ReviewPost reviewPost;

  TextEditingController nameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController middleNameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController companyController = TextEditingController();
  @override
  void initState() {
    reviewPost = ReviewPost(
      city: widget.city,
      isAnonymous: false,
      serviceType: 0,
      feedbackType: 0,
      employee: [
        Employee(),
      ],
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => ReviewBloc(reviewPost),
        child: BlocBuilder<ReviewBloc, ReviewState>(
          builder: (context, state) {
            return GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: WillPopScope(
                onWillPop: () async {
                  if (pageController.page == 0) {
                    Navigator.pop(context);
                  } else {
                    pageController.previousPage(
                        duration: Duration(milliseconds: 300),
                        curve: Curves.easeIn);
                  }
                  return false;
                },
                child: Scaffold(
                  appBar: DefaultAppBar(
                    title: ((state.currentPage == 2 ||
                                state.currentPage == 3) &&
                            state.reviewPost.feedbackType == 1)
                        ? tr('corruptionCase')
                        : ((state.currentPage == 2 || state.currentPage == 3) &&
                                state.reviewPost.feedbackType != 1)
                            ? tr('evaluateSpecialist')
                            : tr('review'),
                    onBackPressed: () {
                      if (pageController.page == 0) {
                        Navigator.pop(context);
                      } else if (pageController.page == 3) {
                      } else {
                        pageController.previousPage(
                            duration: Duration(milliseconds: 300),
                            curve: Curves.easeIn);
                      }
                    },
                    showIcon: state.currentPage != 3,
                  ),
                  body: PageView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: pageController,
                    onPageChanged: (int value) {
                      context.read<ReviewBloc>().updatePage(value);
                    },
                    children: [
                      ReviewFormPart1(
                        formKey: formKey,
                        pageController: pageController,
                        nameController: nameController,
                        lastNameController: lastNameController,
                        phoneController: phoneController,
                        companyController: companyController,
                        middleNameController: middleNameController,
                      ),
                      ReviewFormPart2(
                        pageController: pageController,
                      ),
                      state.reviewPost.feedbackType == 1
                          ? CorruptionPart(
                              pageController: pageController,
                            )
                          : SpecialistRating(
                              pageController: pageController,
                            ),
                      FinalPart(
                        pageController: pageController,
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ));
  }
}

class ReviewFormPart1 extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final PageController pageController;
  final TextEditingController nameController,
      lastNameController,
      middleNameController,
      phoneController,
      companyController;

  ReviewFormPart1({
    this.formKey,
    this.pageController,
    this.companyController,
    this.lastNameController,
    this.middleNameController,
    this.nameController,
    this.phoneController,
  });
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReviewBloc, ReviewState>(
      builder: (context, state) {
        return Container(
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Container(
                  margin: EdgeInsets.only(left: 32.w, right: 32.w, top: 82.h),
                  padding: EdgeInsets.only(bottom: 42.h),
                  child: Form(
                    autovalidateMode: state.autoValidatePart1
                        ? AutovalidateMode.always
                        : AutovalidateMode.disabled,
                    key: formKey,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Theme(
                              data: Theme.of(context).copyWith(
                                unselectedWidgetColor: AppColors.border,
                              ),
                              child: Transform.scale(
                                scale: 1.5,
                                child: Checkbox(
                                  value: state.reviewPost.isAnonymous,
                                  activeColor: AppColors.primaryAlt,
                                  checkColor: Colors.white,
                                  onChanged: (newValue) {
                                    context
                                        .read<ReviewBloc>()
                                        .updateanonymousFill(newValue);
                                  },
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                context.read<ReviewBloc>().updateanonymousFill(
                                    !state.reviewPost.isAnonymous);
                              },
                              child: Container(
                                padding: EdgeInsets.only(left: 16.w),
                                child: AppText(
                                  tr('iwantToFillAnonymous'),
                                  fontSize: Constants.textSize36,
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 30.h),
                        AnimatedSwitcher(
                          duration: Duration(milliseconds: 300),
                          switchInCurve: Curves.easeIn,
                          switchOutCurve: Curves.easeOut,
                          transitionBuilder: (child, animation) {
                            return ScaleTransition(
                              scale: animation,
                              child: child,
                            );
                          },
                          child: state.reviewPost.isAnonymous
                              ? SizedBox.shrink()
                              : Column(
                                  children: [
                                    DefaultTextField(
                                      controller: lastNameController,
                                      hintText: tr('lastName'),
                                      isRequired: true,
                                    ),
                                    SizedBox(height: 30.h),
                                    DefaultTextField(
                                      controller: nameController,
                                      hintText: tr('firstName'),
                                      isRequired: true,
                                    ),
                                    SizedBox(height: 30.h),
                                    DefaultTextField(
                                      controller: middleNameController,
                                      hintText: tr('fatherName'),
                                      isRequired: false,
                                    ),
                                    SizedBox(height: 30.h),
                                    DefaultTextField(
                                      controller: phoneController,
                                      hintText: '',
                                      isRequired: true,
                                      keyboardType: TextInputType.phone,
                                      maxLength: 9,
                                      // leadingText: '+998 ',
                                      leadingIcon: Container(
                                        constraints: BoxConstraints(
                                          maxWidth: 140.w,
                                          minWidth: 120.w,
                                        ),
                                        padding: EdgeInsets.only(left: 32.w),
                                        alignment: Alignment.center,
                                        child: AppText(
                                          '+998',
                                          color: AppColors.lightGrey,
                                          fontSize: Constants.textSize36,
                                        ),
                                      ),
                                      inputFormatters: [
                                        FilteringTextInputFormatter(
                                          RegExp("[1234567890 ]"),
                                          allow: true,
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 30.h),
                                    DefaultTextField(
                                      controller: companyController,
                                      hintText: tr('organizationName'),
                                      isRequired: true,
                                    ),
                                  ],
                                ),
                        ),
                        SizedBox(height: 60.h),
                        GridView(
                          shrinkWrap: true,
                          primary: false,
                          gridDelegate:
                              SliverGridDelegateWithMaxCrossAxisExtent(
                            childAspectRatio: 1,
                            maxCrossAxisExtent: 350.w,
                            mainAxisSpacing: 60.w,
                            crossAxisSpacing: 60.w,
                          ),
                          children: [
                            CheckContainer(
                              index: 0,
                              text: tr('GetA3S'),
                            ),
                            CheckContainer(
                              text: tr('GetOilbase'),
                              index: 1,
                            ),
                            CheckContainer(
                              index: 2,
                              text: tr('GetATNKS'),
                            ),
                            CheckContainer(
                              index: 3,
                              text: tr('GetUsePg'),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SliverFillRemaining(
                hasScrollBody: false,
                child: Container(
                  padding: EdgeInsets.only(bottom: 82.h),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: DefaultFullButton(
                      onPressed: () {
                        if (formKey.currentState.validate()) {
                          context.read<ReviewBloc>().saveUserData(
                                nameController.text,
                                lastNameController.text,
                                middleNameController.text,
                                phoneController.text,
                                companyController.text,
                              );

                          pageController.nextPage(
                              duration: Duration(milliseconds: 200),
                              curve: Curves.ease);
                        } else {
                          context
                              .read<ReviewBloc>()
                              .updateautoValidatePart1(true);
                        }
                      },
                      text: tr('next'),
                      fontSize: Constants.textSize45,
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

class CheckContainer extends StatelessWidget {
  final String text;
  final int index;

  CheckContainer({
    this.text,
    this.index,
  });
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReviewBloc, ReviewState>(
      builder: (context, state) {
        bool checked = state.reviewPost.serviceType == index;
        return GestureDetector(
          onTap: () {
            context.read<ReviewBloc>().updateReviewTopic(index);
          },
          child: AnimatedContainer(
            duration: Duration(milliseconds: 200),
            padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 8.h),
            decoration: BoxDecoration(
              color: checked
                  ? AppColors.primaryAlt.withOpacity(.8)
                  : Colors.transparent,
              borderRadius: BorderRadius.circular(16),
              border: Border.all(
                color: checked
                    ? AppColors.primaryAlt.withOpacity(.8)
                    : AppColors.focusedBorder,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                DefaultCheckBox(checked: checked),
                SizedBox(height: 24.h),
                AppText(
                  text,
                  textAlign: TextAlign.center,
                  fontSize: Constants.textSize32,
                  color: checked ? Colors.white : Colors.black,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
