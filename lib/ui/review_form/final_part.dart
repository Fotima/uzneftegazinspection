import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:uzneftegaz_inspection/bloc/review_bloc.dart';
import 'package:uzneftegaz_inspection/constants/color_constants.dart';
import 'package:uzneftegaz_inspection/constants/constants.dart';
import 'package:uzneftegaz_inspection/constants/image_constants.dart';
import 'package:uzneftegaz_inspection/constants/route_constants.dart';
import 'package:uzneftegaz_inspection/ui/shared/app_text.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_button.dart';

class FinalPart extends StatefulWidget {
  final PageController pageController;

  FinalPart({this.pageController});

  @override
  _FinalPartState createState() => _FinalPartState();
}

class _FinalPartState extends State<FinalPart> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      BlocProvider.of<ReviewBloc>(context).sendRequest();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReviewBloc, ReviewState>(
      builder: (context, state) {
        if (state.loading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          if (state.hasError) {
            Timer(Duration(seconds: 7), () {
              Navigator.pushNamedAndRemoveUntil(
                  context, AppRoutes.launchPage, (route) => false);
            });
          }
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 82.h),
            child: Column(
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height / 2,
                        child: state.hasError
                            ? SvgPicture.asset(
                                AppImages.error_icon,
                                width: 120,
                              )
                            : FlareActor(AppImages.check,
                                alignment: Alignment.center,
                                fit: BoxFit.contain, callback: (String value) {
                                Timer(Duration(seconds: 3), () {
                                  Navigator.pushNamedAndRemoveUntil(context,
                                      AppRoutes.launchPage, (route) => false);
                                });
                              }, animation: "Untitled"),
                      ),
                      AppText(
                        state.hasError
                            ? '${tr('errorMessage')}'
                            : '${tr('reviewIsSent')}!',
                        color: AppColors.lightGrey,
                        fontSize: Constants.textSize64,
                        fontWeight: FontWeight.w600,
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: DefaultFullButton(
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(
                          context, AppRoutes.launchPage, (route) => false);
                    },
                    text: tr('back'),
                    fontSize: Constants.textSize45,
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
