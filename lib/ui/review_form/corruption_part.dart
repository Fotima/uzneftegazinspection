import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:uzneftegaz_inspection/bloc/review_bloc.dart';
import 'package:uzneftegaz_inspection/constants/constants.dart';
import 'package:uzneftegaz_inspection/ui/shared/app_text.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_button.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_text_field.dart';

class CorruptionPart extends StatefulWidget {
  final PageController pageController;

  CorruptionPart({this.pageController});

  @override
  _CorruptionPartState createState() => _CorruptionPartState();
}

class _CorruptionPartState extends State<CorruptionPart> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController middleController = TextEditingController();
  TextEditingController caseDescriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 82.h),
      child: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 30.h),
                  AppText(
                    '${tr('infoAboutSpecialist')}: ',
                    fontSize: Constants.textSize48,
                  ),
                  SizedBox(height: 40.h),
                  DefaultTextField(
                    controller: lastNameController,
                    hintText: tr('lastName'),
                    isRequired: true,
                  ),
                  SizedBox(height: 30.h),
                  DefaultTextField(
                    controller: nameController,
                    hintText: tr('firstName'),
                    isRequired: true,
                  ),
                  SizedBox(height: 30.h),
                  DefaultTextField(
                    controller: middleController,
                    hintText: tr('fatherName'),
                    isRequired: false,
                  ),
                  SizedBox(height: 30.h),
                  DefaultTextField(
                    controller: caseDescriptionController,
                    hintText: tr('caseDescription'),
                    isRequired: true,
                    maxLines: 9,
                  ),
                ],
              ),
            ),
          ),
          SliverFillRemaining(
            hasScrollBody: false,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: DefaultFullButton(
                onPressed: () {
                  if (formKey.currentState.validate()) {
                    context.read<ReviewBloc>().saveCorruptionEmployee(
                          nameController.text,
                          lastNameController.text,
                          middleController.text,
                          caseDescriptionController.text,
                        );

                    widget.pageController.nextPage(
                        duration: Duration(milliseconds: 200),
                        curve: Curves.ease);
                  }
                },
                text: tr('send'),
                fontSize: Constants.textSize45,
              ),
            ),
          )
        ],
      ),
    );
  }
}
