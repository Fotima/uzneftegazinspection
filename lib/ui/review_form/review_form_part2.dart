import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uzneftegaz_inspection/bloc/review_bloc.dart';
import 'package:uzneftegaz_inspection/constants/color_constants.dart';
import 'package:uzneftegaz_inspection/constants/constants.dart';
import 'package:uzneftegaz_inspection/ui/shared/app_text.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_button.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_check_box.dart';

class ReviewFormPart2 extends StatelessWidget {
  final PageController pageController;

  ReviewFormPart2({this.pageController});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReviewBloc, ReviewState>(
      builder: (context, state) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 82.h),
          child: Column(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CheckBoxContainer(
                      value: state.reviewPost.feedbackType == 0,
                      text: tr('evaluateSpecialist'),
                      onPressed: () {
                        context.read<ReviewBloc>().updateFeedbackType(0);
                      },
                    ),
                    SizedBox(height: 52.h),
                    CheckBoxContainer(
                      value: state.reviewPost.feedbackType == 1,
                      onPressed: () {
                        context.read<ReviewBloc>().updateFeedbackType(1);
                      },
                      text: tr('facedCurroption'),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: DefaultFullButton(
                  onPressed: () {
                    pageController.nextPage(
                        duration: Duration(milliseconds: 300),
                        curve: Curves.ease);
                  },
                  text: tr('next'),
                  fontSize: Constants.textSize45,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class CheckBoxContainer extends StatelessWidget {
  final bool value;
  final String text;
  final VoidCallback onPressed;

  CheckBoxContainer({
    this.text,
    this.value,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPressed();
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 52.h),
        decoration: BoxDecoration(
          border: Border.all(color: AppColors.focusedBorder),
          borderRadius: BorderRadius.circular(16),
          color:
              value ? AppColors.primaryAlt.withOpacity(.8) : Colors.transparent,
        ),
        child: Row(
          children: [
            DefaultCheckBox(
              checked: value,
              size: 80.w,
            ),
            SizedBox(width: 32.w),
            AppText(
              text,
              fontSize: Constants.textSize40,
              color: value ? Colors.white : Colors.black,
            )
          ],
        ),
      ),
    );
  }
}
