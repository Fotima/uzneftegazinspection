import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:uzneftegaz_inspection/bloc/review_bloc.dart';
import 'package:uzneftegaz_inspection/constants/color_constants.dart';
import 'package:uzneftegaz_inspection/constants/constants.dart';
import 'package:uzneftegaz_inspection/models/review_on_specialist.dart';
import 'package:uzneftegaz_inspection/ui/shared/app_text.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_button.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_flushbar.dart';
import 'package:uzneftegaz_inspection/ui/shared/default_text_field.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SpecialistRating extends StatefulWidget {
  final PageController pageController;

  SpecialistRating({this.pageController});
  @override
  _SpecialistRatingState createState() => _SpecialistRatingState();
}

class _SpecialistRatingState extends State<SpecialistRating> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  List<String> specialists = [];
  @override
  void initState() {
    specialists = [
      'Бердибаев Мирас Кенгесбаевич',
      'Таджибаев Уткир Хекматуллаевич',
      'Хамроев Бекзод Ҳамзаевич',
      'Эргашев Азиз Мамарасулович',
      'Махсумов Икром Эргашевич',
      'Мамадалиев Анвар Тоирович',
      'Пардаев Ахмаджон Искандарович',
      'Мустафокулов Шерали Худойназарович',
      'Мухамадиев Хушбак Кадирович',
      'Бозоров Сардор Рустамович',
      'Аминов Нуриддин Наврузович',
      'Махмудов Фируз Махмудович',
      'Ахмедов Элмурод Абдирахманович',
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 32.w),
        child: BlocBuilder<ReviewBloc, ReviewState>(
          builder: (context, state) {
            return CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                    child: Column(
                  children: [
                    SizedBox(height: 80.h),
                    Form(
                      key: formKey,
                      child: Column(
                          children: state.reviewPost.employee
                              .map((e) => SpecialistForm(
                                    specialists: specialists,
                                    index: state.reviewPost.employee.indexOf(e),
                                    // reviewOnSpecialist: e,
                                  ))
                              .toList()),
                    ),
                    // ListView.builder(
                    //     shrinkWrap: true,
                    //     primary: false,
                    //     itemCount: state.specialists.length,
                    //     itemBuilder: (context, index) {
                    //       return Column(
                    //         children: [
                    //           SpecialistForm(
                    //             specialists: specialists,
                    //             index: index,
                    //             reviewOnSpecialist: state.specialists[index],
                    //           ),
                    //           state.specialists.length > 1
                    //               ? Divider(
                    //                   color: AppColors.focusedBorder,
                    //                 )
                    //               : Container()
                    //         ],
                    //       );
                    //     }),
                    SizedBox(height: 80.h),
                    AddSpecialistRatingButton(),
                  ],
                )),
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 80.h),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: DefaultFullButton(
                        onPressed: () {
                          if (formKey.currentState.validate()) {
                            if (state.reviewPost.employee.any((element) =>
                                element.mark == 0 || element.mark == null)) {
                              showMessage(tr('pleaseSetRating'), true, context);
                            } else {
                              FocusScope.of(context).unfocus();
                              context
                                  .read<ReviewBloc>()
                                  .udpateReview(state.reviewPost);
                              widget.pageController.nextPage(
                                  duration: Duration(milliseconds: 200),
                                  curve: Curves.ease);
                            }
                          }
                          // if (state.specialists
                          //     .any((element) => element.fullName == null)) {
                          //   showMessage(tr('pleaseSelectSpecialistToRate'),
                          //       true, context);
                          // } else if (state.specialists.any((element) =>
                          //     element.rate == 0 || element.rate == null)) {
                          //   showMessage(tr('pleaseSetRating'), true, context);
                          // } else {
                          //   widget.pageController.nextPage(
                          //       duration: Duration(milliseconds: 200),
                          //       curve: Curves.ease);
                          // }
                        },
                        text: tr('send'),
                        fontSize: Constants.textSize45,
                      ),
                    ),
                  ),
                )
              ],
            );
          },
        ));
  }
}

class SpecialistForm extends StatelessWidget {
  final List<String> specialists;
  final int index;

  SpecialistForm({
    this.specialists,
    this.index,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReviewBloc, ReviewState>(
      builder: (context, state) {
        return Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30.h),
              Row(
                children: [
                  Expanded(
                    child: AppText(
                      '${tr('infoAboutSpecialist')}: ',
                      fontSize: Constants.textSize48,
                    ),
                  ),
                  index == 0
                      ? Container()
                      : IconButton(
                          icon: Icon(Icons.close),
                          onPressed: () {
                            context
                                .read<ReviewBloc>()
                                .removeSpecialistReview(index);
                          })
                ],
              ),
              SizedBox(height: 40.h),
              DefaultTextField(
                controller: null,
                hintText: tr('lastName'),
                isRequired: true,
                initialValue: state.reviewPost.employee[index].lastName,
                onChanged: (String value) {
                  state.reviewPost.employee[index].lastName = value;
                },
              ),
              SizedBox(height: 30.h),
              DefaultTextField(
                controller: null,
                hintText: tr('firstName'),
                isRequired: true,
                initialValue: state.reviewPost.employee[index].firstName,
                onChanged: (String value) {
                  state.reviewPost.employee[index].firstName = value;
                },
              ),
              SizedBox(height: 30.h),
              DefaultTextField(
                controller: null,
                hintText: tr('fatherName'),
                isRequired: false,
                initialValue: state.reviewPost.employee[index].middleName,
                onChanged: (String value) {
                  state.reviewPost.employee[index].middleName = value;
                },
              ),
              // NameDropDown(
              //   specialists: specialists,
              //   onSelected: (String value) {
              //     reviewOnSpecialist.fullName = value;
              //   },
              //   value: reviewOnSpecialist.fullName,
              // ),
              SizedBox(height: 60.h),
              AppText(
                '${tr('yourRate')}: ',
                fontSize: Constants.textSize48,
              ),
              SizedBox(height: 40.h),
              RatingBar(
                initialRating: state.reviewPost.employee[index].mark == null
                    ? 0
                    : state.reviewPost.employee[index].mark.toDouble(),
                direction: Axis.horizontal,
                allowHalfRating: false,
                itemCount: 5,
                itemSize: 70,
                ratingWidget: RatingWidget(
                  full: Icon(
                    Icons.star,
                    color: AppColors.accentColor,
                  ),
                  half: Icon(
                    Icons.star,
                    color: AppColors.accentColor,
                  ),
                  empty: Icon(Icons.star_border),
                ),
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                onRatingUpdate: (rating) {
                  state.reviewPost.employee[index].mark = rating.toInt();
                },
              ),
              SizedBox(height: 80.h),
              DefaultTextField(
                controller: null,
                clearIfNulll: true,
                initialValue: state.reviewPost.employee[index].comment,
                hintText: tr('comment'),
                isRequired: false,
                maxLines: 9,
                onChanged: (String value) {
                  state.reviewPost.employee[index].comment = value;
                },
              ),
              SizedBox(height: 40.h),
            ],
          ),
        );
      },
    );
  }
}

class NameDropDown extends StatelessWidget {
  final List<String> specialists;
  final Function onSelected;
  final String value;

  NameDropDown({
    this.specialists,
    this.onSelected,
    this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context)
          .copyWith(cardColor: Colors.white, textTheme: TextTheme()),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 12.w),
        decoration: BoxDecoration(
            border: Border.all(color: AppColors.border),
            borderRadius: BorderRadius.circular(12)),
        child: SearchableDropdown.single(
          disabledHint: 'Hint',
          displayClearIcon: false,
          closeButton: DefaultFullButton(
            onPressed: () {
              Navigator.pop(context);
            },
            text: tr('close'),
            fontSize: 20,
          ),
          icon: Icon(
            Icons.keyboard_arrow_down,
            color: AppColors.iconColor,
          ),
          style: TextStyle(color: Colors.black),
          clearIcon: Icon(
            Icons.clear,
            color: Colors.black,
          ),
          underline: Container(
            color: AppColors.primaryColor,
          ),
          items: specialists
              .map((e) => DropdownMenuItem(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 12.h),
                      child: AppText(
                        '$e',
                        fontSize: Constants.textSize36,
                      ),
                    ),
                    value: '$e',
                  ))
              .toList(),
          value: value,
          hint: Container(
            padding: EdgeInsets.symmetric(vertical: 24.h),
            child: AppText(
              tr('selectSpecialist'),
              color: AppColors.lightGrey,
              fontSize: Constants.textSize36,
            ),
          ),
          searchHint: tr('selectSpecialist'),
          onChanged: (value) {
            onSelected(value);
          },
          isExpanded: true,
        ),
      ),
    );
  }
}

class AddSpecialistRatingButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.read<ReviewBloc>().addSpecialistReview();
      },
      child: Container(
        child: Row(
          children: [
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: AppColors.focusedBorder),
              ),
              child: Icon(
                Icons.add,
                color: AppColors.focusedBorder,
                size: 35,
              ),
            ),
            SizedBox(width: 24.w),
            Expanded(
              child: AppText(
                tr('rateOneMoreSpecialist'),
                fontSize: Constants.textSize48,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
