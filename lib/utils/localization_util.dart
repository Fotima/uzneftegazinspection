import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

abstract class LocalizationUtil {
  static const List<String> supportedLanguages = [
    'Русский',
    "O'zbek",
    // 'Deutsche'
  ];

  // static const List<String> supportedLanguagesCodes = ['ru', 'en', 'de'];
  // static const List<String> supportedLanguagesCountryCodes = ['RU', 'US', 'DE'];

  static const List<String> supportedLanguagesCodes = [
    'ru',
    'uz',
  ];
  static const List<String> supportedLanguagesCountryCodes = [
    'RU',
    'UZ',
  ];

  static List<Locale> supportedLocales() => supportedLanguagesCodes
      .map<Locale>((language) => Locale(
          language,
          supportedLanguagesCountryCodes[
              supportedLanguagesCodes.indexOf(language)]))
      .toList();

  static String currentLangCode(BuildContext context) {
    String currentLocale = context.locale.toString();
    if (currentLocale.length > 2) {
      if (currentLocale[2] == '-' || currentLocale[2] == '_') {
        currentLocale = currentLocale.substring(0, 2);
      }
    }
    return currentLocale;
  }

  static String currentLang(BuildContext context) {
    String currentLocale = context.locale.toString();
    String currentLanuage = '';
    if (currentLocale.length > 2) {
      if (currentLocale[2] == '-' || currentLocale[2] == '_') {
        currentLocale = currentLocale.substring(0, 2);
      }
    }
    if (supportedLanguagesCodes.any((element) => element == currentLocale) &&
        supportedLanguagesCodes.length == supportedLanguages.length) {
      currentLanuage =
          supportedLanguages[supportedLanguagesCodes.indexOf(currentLocale)];
    }
    return currentLanuage;
  }

  static void updateLocal(BuildContext context, String lang) {
    context.locale = Locale(lang,
        supportedLanguagesCountryCodes[supportedLanguagesCodes.indexOf(lang)]);
  }
}
