import 'package:shared_preferences/shared_preferences.dart';

// ignore: avoid_classes_with_only_static_members
class SharedPrefHelper {
  static SharedPreferences prefs;
  static const lang = 'language';
  static Future<void> init() async {
    prefs ??= prefs = await SharedPreferences.getInstance();
  }

  static void dispose() => prefs = null;
  static Future<bool> clear() => prefs.clear();

  static String get language => prefs.getString(lang);
  static set language(String value) => prefs.setString(lang, value);
}
