import 'package:flutter/material.dart';
import 'package:uzneftegaz_inspection/constants/route_constants.dart';
import 'package:uzneftegaz_inspection/ui/launch_page.dart';
import 'package:uzneftegaz_inspection/ui/review_form/review_form.dart';

class MainRouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AppRoutes.launchPage:
        return MaterialPageRoute(
          builder: (_) => LaunchPage(),
        );
      case AppRoutes.reviewForm:
        return MaterialPageRoute(
          builder: (_) => ReviewForm(
            city: settings.arguments,
          ),
        );
      default:
        return null;
      // return _errorRoute();
    }
  }
}
