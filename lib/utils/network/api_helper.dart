import 'package:chopper/chopper.dart';
import 'package:http/http.dart' as http;

import 'connectivity.dart';

Future request(Future request) async {
  bool connected = await ConnectivityChecker.isConnected();

  if (connected) {
    dynamic data = request.timeout(
      Duration(seconds: 30),
      onTimeout: () => Response(
        http.Response('', 408),
        null,
        error: 'Time out error',
      ),
    );
    return data;
  } else {
    return Response(
      http.Response('', 503),
      null,
      error: 'No internet connection',
    );
  }
}
