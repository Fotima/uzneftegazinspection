import 'package:connectivity/connectivity.dart';

class ConnectivityChecker {
  static Connectivity _connectivity = new Connectivity();

  static Future<bool> isConnected() async {
    ConnectivityResult connectionStatus =
        await _connectivity.checkConnectivity();
    return connectionStatus != ConnectivityResult.none;
  }
}
