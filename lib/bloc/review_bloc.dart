import 'package:chopper/chopper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uzneftegaz_inspection/api/api_provider.dart';
import 'package:uzneftegaz_inspection/models/api/review_post.dart';
import 'package:uzneftegaz_inspection/utils/network/api_helper.dart';

abstract class ReviewEvent {}

class ReviewState {
  final bool autoValidatePart1;
  final int currentPage;
  final ReviewPost reviewPost;
  final bool loading;
  final bool hasError;

  ReviewState({
    this.autoValidatePart1,
    this.currentPage,
    this.reviewPost,
    this.loading,
    this.hasError,
  });

  ReviewState copyWith({
    bool autoValidatePart1,
    int currentPage,
    ReviewPost reviewPost,
    bool loading,
    bool hasError,
  }) {
    return ReviewState(
      autoValidatePart1: autoValidatePart1 ?? this.autoValidatePart1,
      currentPage: currentPage ?? this.currentPage,
      reviewPost: reviewPost ?? this.reviewPost,
      loading: loading ?? this.loading,
      hasError: hasError ?? this.hasError,
    );
  }
}

class ReviewBloc extends Cubit<ReviewState> {
  ReviewBloc(ReviewPost reviewPost)
      : super(ReviewState(
            autoValidatePart1: false,
            reviewPost: reviewPost,
            loading: false,
            hasError: false,
            currentPage: 0));

  void updateanonymousFill(bool value) {
    state.reviewPost.isAnonymous = value;
    emit(state.copyWith(reviewPost: state.reviewPost));
  }

  void updateReviewTopic(int value) {
    state.reviewPost.serviceType = value;
    emit(state.copyWith(reviewPost: state.reviewPost));
  }

  void updateautoValidatePart1(bool value) {
    emit(state.copyWith(autoValidatePart1: value));
  }

  void updateFeedbackType(int value) {
    state.reviewPost.feedbackType = value;

    emit(state.copyWith(reviewPost: state.reviewPost));
  }

  void updatePage(int value) {
    emit(state.copyWith(currentPage: value));
  }

  void addSpecialistReview() {
    state.reviewPost.employee.add(Employee());
    emit(state.copyWith(reviewPost: state.reviewPost));
  }

  void saveUserData(String name, String lastName, String middleName,
      String phone, String company) {
    state.reviewPost.firstName = name;
    state.reviewPost.lastName = lastName;
    state.reviewPost.middleName = middleName;
    state.reviewPost.phone = '+998$phone';
    state.reviewPost.companyName = company;

    emit(state.copyWith(
      reviewPost: state.reviewPost,
    ));
  }

  void saveCorruptionEmployee(
      String name, String lastName, String middleName, String caseDescription) {
    state.reviewPost.employee[0] = Employee(
        firstName: name,
        lastName: lastName,
        middleName: middleName,
        comment: caseDescription);

    emit(state.copyWith(reviewPost: state.reviewPost));
  }

  void removeSpecialistReview(int index) {
    state.reviewPost.employee.removeAt(index);
    emit(state.copyWith(
      reviewPost: state.reviewPost,
    ));
  }

  void udpateReview(ReviewPost reviewPost) {
    emit(state.copyWith(reviewPost: reviewPost));
  }

  void sendRequest() async {
    emit(state.copyWith(loading: true));
    ApiProvider.create();
    Response response =
        await request(ApiProvider.formApi.sendReview(state.reviewPost));
    if (response.isSuccessful) {
      emit(state.copyWith(loading: false));
    } else {
      emit(state.copyWith(loading: false, hasError: true));
    }
  }
}
